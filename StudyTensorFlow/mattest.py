import tensorflow as tf

w = tf.Variable([[1, 1, 1], [1, 1, 1,]])
x = tf.Variable([[1, 1],[1, 1], [1, 1]])
b = tf.Variable([1, 1])
data = tf.matmul(w, x)
#首先，偏置确实和权重与x的乘积的维度不同
#其次，tf.nn.bias_add()和加号操作的结果没有差别
result = tf.nn.bias_add(data, b)
result2 = data + b
with tf.Session() as sess:
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    data_output = sess.run(data)
    print(data_output)
    result_output = sess.run(result)
    print(result_output)
    result2_output = sess.run(result2)
    print(result2_output)
