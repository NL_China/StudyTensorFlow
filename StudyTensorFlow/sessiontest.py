import tensorflow as tf

x = tf.placeholder(tf.float16)
y = tf.placeholder(tf.float16)

z = x + y

m = tf.placeholder(tf.int32, shape=[2])
n = tf.placeholder(tf.int32, shape=[2])
l = tf.multiply(m, n)

with tf.Session() as sess:
    #Session对象并不像我想象的一次会话只能执行一次，可以多次执行
    #执行run的返回值就是Tensor(Operation)的值，而Tensor本身代表的是操作
    #Session.run()就相当于Tensor.eval()
    ret = sess.run(z, feed_dict={x: 1.0, y: 2.0})
    print(ret)

    ret2 = sess.run(l, feed_dict={m: [1, 2], n: [3, 4]})
    print(ret2)