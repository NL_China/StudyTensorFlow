import tensorflow as tf

with tf.name_scope('a') as ns:
    print(ns)

with tf.variable_scope('b') as vs:
    print(vs)