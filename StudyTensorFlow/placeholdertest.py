import tensorflow as tf

x = tf.placeholder(tf.float16, name='x')
y = tf.placeholder(tf.float16, name='y')
op1 = tf.multiply(x, y)
with tf.Session() as sess:
    #不能相信VS的提示，op1对象根本没有run()这个成员函数
    #ret = op1.run(feed_dict={x: 1.0, y: 1.1}, session=sess)
    #更让人无法理解的是，eval()作为官网API文档中的Tensor类的成员函数，VS竟然提示不存在
    ret = op1.eval(feed_dict={x: 1.0, y: 1.1}, session=sess)
    print(ret)
    

'''
placeholder类型都是不能trainable的
'''