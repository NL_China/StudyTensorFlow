import tensorflow as tf

#激活函数sign，输入值等于0，返回0；大于0，返回1,；小于0，返回-1；
ze = tf.sign(0)
zm = tf.sign(10)
zl = tf.sign(-9)
#激活函数sigmoid()，计算1 / (1 + exp(-x))
se = tf.sigmoid(0.0)
sm = tf.sigmoid(10.0)
sl = tf.sigmoid(-9.0)
#激活函数tf.nn.relu()，取0和输入值之间哪个大就返回哪个
re = tf.nn.relu(0.0)
rm = tf.nn.relu(10.0)
rl = tf.nn.relu(-9.0)
with tf.Session() as sess:
    v = sess.run([ze, zm, zl, se, sm, sl, re, rm, rl])
    print(v)