import tensorflow as tf

v = tf.Variable(1.1,  name='var')
v1 = tf.get_variable(name='var1', shape=(1, 2))
v2 = tf.Variable(1.2)
v3 = tf.Variable(1.3)
with tf.name_scope('ns1'):
    v4 = tf.Variable(1.4, name='var4')
    v5 = tf.Variable(1.5, name='var5')
    #tf.name_scope() 并不会对 tf.get_variable() 创建的变量有任何影响。 
    v6 = tf.get_variable(name='var6', shape=[1])
    ph1 = tf.placeholder(tf.float16, name='ph1')
    ph11 = tf.placeholder(tf.float16, name='ph1')

with tf.name_scope('ns2'):
    with tf.variable_scope('vs2'):
        v7 = tf.Variable(1.7, name='var7')
        v8 = tf.get_variable(name='var8', shape=[0])
        ph2 = tf.placeholder(tf.float16, name='ph2')
        ph21 = tf.placeholder(tf.float16, name='ph2')

with tf.variable_scope('vs3'):
    with tf.name_scope('ns3'):
        v9 = tf.Variable(1.9, name='var9')
        v10 = tf.get_variable(name='var10', shape=[0])

with tf.name_scope('ns4'):
    with tf.variable_scope('vs4'):
        v11 = tf.Variable(1.11, name='var11')
        v11_1 = tf.Variable(1.11, name='var11')
        v12 = tf.get_variable(name='var12', shape=[0])
        #下面这行如果打开会报错，因为同一variable scope下已经有了同名的变量了
        #v12_1 = tf.get_variable(name='var12', shape=[0])

with tf.variable_scope('vs5'):
    v12_2 = tf.get_variable(name='var12', shape=[0])

v12_3 = tf.get_variable(name='var12', shape=[0])

v13 = tf.Variable([1, 2], tf.int32)

vDefault = tf.Variable(2.0)

global_init = tf.global_variables_initializer()
with tf.Session() as sess:    
    # Run the Op that initializes global variables.
    sess.run(global_init)
    print('v value: ', v.eval())
    print('v1 value: ', v1.eval())
    #v1和v6都是通过get_variable()定义的，通过输可以看到其变量名和name_scope()中定义的命名空间无关
    print('v1 name: ', v1.name)
    #v2, v3是在name_scope外面定义的，所以name里面不带有ns1
    print('v2 name: ', v2.name)
    print('v3 name: ', v3.name)
    #v4, v5是在name_scope里面定义的，所以name里面带有ns1
    print('v4 name: ', v4.name)
    print('v5 name: ', v5.name)
    #v6是通过get_variable定义的，所以v6.name里面不带有name_scope设置的命名空间ns1
    print('v6 name: ', v6.name)
    #v7和v8定义在name_scope: ns1下的variable_scope: vs1下
    #name_scope和variable_scope定义的命名空间会一起作用在Variable定义的变量上
    print('v7 name: ', v7.name)
    #和name_scope: ns1下的v6变量相同，name_scope不作用在get_variable()定义的变量上
    print('v8 name: ', v8.name)
    #对于v9来说，仍旧是name_scope和variable_scope定义的命名空间会一起作用在它之上，但是由于是先定义variable_scope再定义name_scope，所以命名空间也按照这个顺序
    print('v9 name: ', v9.name)
    print('v10 name: ', v10.name)
    #对于Variable定义的变量，如果名字相同，那么自动会自动给第二个变量改名
    print('v11 name: ', v11.name)
    print('v11_1 name: ', v11_1.name)
    #下面三个变量在定义的时候虽然name都是var12，但是由于在不同的variable scope下，因此变量都能初始化
    print('v12 name: ', v12.name)
    #print('v12_1 name: ', v12_1.name)
    print('v12_2 name: ', v12_2.name)
    print('v12_3 name: ', v12_3.name)
    print('v13 value', v13.eval())

    #placeholder变量和Variable变量的行为完全相同
    print(ph1.name)
    print(ph11.name)
    print(ph2.name)
    print(ph21.name)

'''
tf.name_scope() 主要是用来管理命名空间的，这样子让我们的整个模型更加有条理。
tf.variable_scope() 的作用是为了实现变量共享，它和 tf.get_variable() 来完成变量共享的功能。
'''