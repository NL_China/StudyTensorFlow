import random

data_pos = []
data_neg = []
for i in range(10000):
    x = random.uniform(20.0, 90.0)
    y = random.uniform(20.0, 90.0)
    if (x + y < 95.0) & (len(data_pos) < 50):
        data_pos.append([x, y, 1])
    elif (x + y > 105.0) & (len(data_neg) < 50):
        data_neg.append([x, y, 0])
    if len(data_pos) + len(data_neg) == 100:
        break

datafile = open('D:\ex2data1_new.txt', 'w')
for datapos in data_pos:
    print(str(datapos[0])+','+str(datapos[1])+','+str(datapos[2]), file = datafile)
for dataneg in data_neg:
    print(str(dataneg[0])+','+str(dataneg[1])+','+str(dataneg[2]), file = datafile)
