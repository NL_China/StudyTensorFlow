import csv
import os
import numpy as np


def readCsvData(folderpath, filename, toprint, tofloat):
    all_data = []
    filepath = os.path.join(folderpath, filename)
    #通过python的csv模块读取csv文件，而不是通过tensorflow读取csv文件
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            if toprint:
                print(row)
            newrow = []
            cols = len(row)
            for col in range(cols):
                if tofloat:
                    newrow.append(float(row[col]))
                else:
                    newrow.append(row[col])
            all_data.append(newrow)

    all_array = np.array(all_data)
    np.random.shuffle(all_array)
    return all_array

def normalize(data_value, data_cols):
    data_rows = len(data_value)
    for i in range(data_cols):
        all_col_array = data_value[:, i]
        data_col_mean = all_col_array.mean()
        data_col_standard_deviation = all_col_array.std()
        for j in range(data_rows):
            data_value[j][i] = (data_value[j][i] - data_col_mean) / data_col_standard_deviation
    return data_value