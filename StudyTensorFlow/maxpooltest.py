import tensorflow as tf

a = tf.constant([
[[1.0, 2.0, 3.0, 4.0],
 [5.0, 6.0, 7.0, 8.0],
 [8.0, 7.0, 6.0, 5.0],
 [4.0, 3.0, 2.0, 1.0]],
[[4.0, 3.0, 2.0, 1.0],
 [8.0, 7.0, 6.0, 5.0],
 [1.0, 2.0, 3.0, 4.0],
 [5.0, 6.0, 7.0, 8.0]]
])
#a的shape是[2, 4, 4]
print(a.shape)
#reshape成[1, 4, 4, 2]
a = tf.reshape(a, [1, 4, 4, 2])
#pooling的kernal大小是2x2的，在第2和第3两个维度上进行pooling，strides的横向和纵向都是1，padding设置的是VALID
#a中间的两个维度是4x4的，那么pooling后的结果就是4-2+1=3，也就是3x3的数据，那么pooling的结果的shape就是[1, 3, 3, 2]
poolinga = tf.nn.max_pool(a, [1, 2, 2, 1], [1, 1, 1, 1], padding='VALID')
#在中间两个维度进行pooling不太好想象，可以先把中间两个维度通过transpose挪到最后两个维度，同时pooling的kernel也改为[1, 1, 2, 2]
#得到pooling后的结果再将后两维数据transpose回到中间
b = tf.transpose(a, [0, 3, 1, 2])
#这种写法会报错：MaxPooling supports exactly one of pooling across depth or pooling across width/height.
#max_pooling()只支持在中间两维也就是图像的宽高，或者最后一维也就是图像的不同的通道上进行pooling，不能像下面传入的kernel那样在图像的宽度和通道上pooling
#poolingb = tf.nn.max_pool(b, [1, 1, 2, 2], [1, 1, 1, 1], padding='VALID')
#poolingb_transpose = tf.transpose(poolingb, [0, 2, 3, 1])
#手动推断一下poolingb的结果为：
#[[[8, 6, 7]
#  [8, 8, 8]
#  [4, 8, 8]]
# [[7, 6, 8]
#  [7, 7, 7]
#  [4, 7, 8]]]
#上面这个数组的shape是[1, 2, 3, 3]
#然后进行transpos，将上面这个数组的shape变为[1, 3, 3, 2]，其实这个transpose操作就是把第2个维度上的数据拼一起，例如[0, 0, 0, 0]和[0, 1, 0, 0]，也就是8和7
#新的shape的第3维度是3，这个维度就是transpose之前的数组的最后一个维度。看上面的数组就是横向地遍历每一列。transpose后变成了第3维度，那么就应该按照这个维度继续进行transpose，也就是[0, 0, 0, 1]和[0, 1, 0, 1]，也就是6和6
#然后[0, 0, 0, 2]和[0, 1, 0, 2]，也就是7和8，这样就完成了一个[3, 2]的数组。
#列遍历完了，该遍历行了。那就是将[8, 8, 8]和[7, 7, 7]的同一位置上的元素组合在一起组成3个[8, 7]
#strides能否比pooling kernel大?
#运行结果没有报错，说明strides设置得比pooling kernel大没有问题，这意味着放弃了一些数据
pooling1 = tf.nn.max_pool(a, [1, 2, 2, 1], [1, 3, 3, 1], padding='VALID')
pooling2 = tf.nn.max_pool(a, [1, 2, 2, 1], [1, 3, 3, 1], padding='SAME')
#padding的VALID和SAME有什么不同效果?
#从上面的实验看，SAME就是加了padding，这样能保证最后几列像素不足以pooling的时候通过补padding还能做一次pooling
#SAME不是说pooling之后的feature map大小和原图一样，5x5的图像，3x3的pooling kernel，设置padding为SAME，得到的feature map大小也是3x3，这是一个巧合。

with tf.Session() as sess:  
    print('image a:')
    imagea = sess.run(a)
    print(imagea)
    print('result a:')
    resulta = sess.run(poolinga)
    print(resulta)
    print('image b:')
    imageb = sess.run(b)
    print(imageb)
    #print('result b:')
    #resultb = sess.run(poolingb)
    #print(resultb)
    #print('result b_tranpose:')
    #resultb_transpose = sess.run(poolingb_transpose)
    #print(resultb_transpose)
    print('result pooling1 shape:')
    result1 = sess.run(pooling1)
    print(result1.shape)
    print('result pooling2 shape:')
    result2 = sess.run(pooling2)
    print(result2.shape)