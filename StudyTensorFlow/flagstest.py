import tensorflow as tf

FLAGS=tf.app.flags.FLAGS

#tf.app.flags.DEFINE_float()等函数内部其实调用的就是python的argpaese，通过argparse.ArgumentParser().add_argument()添加程序的参数
tf.app.flags.DEFINE_float(  
    'flag_float', 0.01, 'input a float')  
tf.app.flags.DEFINE_integer(  
    'flag_int', 400, 'input a int')  
tf.app.flags.DEFINE_boolean(  
    'flag_bool', True, 'input a bool')  
tf.app.flags.DEFINE_string(  
    'flag_string', 'yes', 'input a string')  
  
print(FLAGS.flag_float)  
print(FLAGS.flag_int)  
print(FLAGS.flag_bool)  
print(FLAGS.flag_string) 
