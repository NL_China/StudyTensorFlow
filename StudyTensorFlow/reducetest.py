import tensorflow as tf
import numpy as np

data = np.arange(10).reshape(5,2)
print(data)
#keep_dims为false，代表执行后得出的值是一个数值
#没有指定axis代表多维数组所有成员之和
reducesum0 = tf.reduce_sum(data, keep_dims=False)
#keep_dims为true，代表执行后得出的值和输入的数据对象的维度相同
reducesum1 = tf.reduce_sum(data, keep_dims=True)
#axis和normalize里面的dims类似，对于二维数组，1代表最后一层sum，所以结果是5个值，0代表的是第一维度的数据的sum，所以是2个值
#输出结果都是一维数组
reducesum2 = tf.reduce_sum(data, 0)
reducesum3 = tf.reduce_sum(data, 1)
#如果加上keep_dims=true，axis=0输出1x2的数组，axis=1输出5x1的数组，原数组是5x2
reducesum4 = tf.reduce_sum(data, 0, keep_dims=True)
reducesum5 = tf.reduce_sum(data, 1, keep_dims=True)
with tf.Session() as sess:
    print(sess.run(reducesum0))
    print(sess.run(reducesum1))
    print(sess.run(reducesum2))
    print(sess.run(reducesum3))
    print(sess.run(reducesum4))
    print(sess.run(reducesum5))
