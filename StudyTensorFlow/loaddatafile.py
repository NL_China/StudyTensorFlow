import tensorflow as tf

#一开始参数给的是/data/ex1data1.txt，报错：The system cannot find the path specified. 改成绝对路径就好了，不过还是要研究一下相对路径的给法
filename_queue = tf.train.string_input_producer(["D:/Tools/projects/StudyTensorFlow/StudyTensorFlow/excise/ex1/ex1data1.txt"])
reader = tf.TextLineReader()
key, value = reader.read(filename_queue)
#如果csv文件是M行N列，那么record_defaults就应该是一个N*1的数组，1.0代表数据是float，1代表是int，string不清楚
record_defaults = [[1.0], [1.0]]
#csv文件中有几列，这里就要写几个参数
col1, col2 = tf.decode_csv(
    value, record_defaults=record_defaults)

with tf.Session() as sess:
    # Start populating the filename queue.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    x = []
    y = []
    for i in range(97):
        # Retrieve a single instance:
        ret = sess.run([col1, col2])
        x.append(ret[0])
        y.append(ret[1])
    coord.request_stop()
    coord.join(threads)
'''
一般一行里面的值会用逗号或者空格隔开，这里第三个输入参数就是指定用什么来进行分割，默认为逗号。第二个输入参数是指定分割后每个属性的类型，比如分割后会有三列，那么第二个参数就应该是[['int32'], [], ['string']], 可见不指定类型（设为空[]）也可以。如果分割后的属性比较多，比如有100个，可以用[ []*100 ]来表示
col= tf.decode_csv(records, record_defaults=[ [ ]*100 ], field_delim=‘ ’, name=None)
'''