import tensorflow as tf

#初始化每个成员都为0的array，注意不是list
init = tf.zeros([10])
p = []
p.append(0)
p.append(1)
v = tf.Variable(initial_value=init)
with tf.Session() as sess:
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    ret = sess.run(v)
    print('value: ', ret)
    print(p)
