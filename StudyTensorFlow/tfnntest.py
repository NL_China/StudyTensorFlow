#测试tf.nn命名空间下的函数
import tensorflow as tf
import numpy as np

data1 = tf.Variable([[0.1, 0.2, 0.3], [1.1, 1.2, 1.3], [2.1, 2.2, 2.3], [3.1, 3.2, 3.3], [4.1, 4.2, 4.3]])
data2 = tf.Variable([10.1, 20.5, 30.4, 50.9], tf.float16)
indexes1 = tf.Variable([1, 3], tf.int32)
#索引可以是任意维度，但是只会去目标tensor list中用每一个索引到tensor list的第一维查找并取出数据
indexes2 = tf.Variable([[0, 2], [1, 2]], tf.int32)
indexes3 = tf.Variable([1, 2, 1, 3], tf.int32)
indexes4 = [0, 3, 2, 1, 2]

out1 = tf.nn.embedding_lookup(data1, indexes1)
out2 = tf.nn.embedding_lookup(data1, indexes2)
#第一个参数必须是tensor，第二个参数可以是tensor，也可以是numpy数组或者python list
out3 = tf.nn.embedding_lookup(data2, indexes3)
out4 = tf.nn.embedding_lookup(data2, indexes4)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    print(sess.run(out1))
    print(out1)
    print(sess.run(out2))
    print(out2)
    print(sess.run(out3))
    print(out3)
    print(sess.run(out4))
    print(out4)
    print('==================')
    #print(sess.run(out2))
    #print(out2)

