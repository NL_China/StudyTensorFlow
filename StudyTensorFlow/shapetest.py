import tensorflow as tf

#变量的shape可以通过list或者tuple来赋值，参数中shape的类型是tuple，而且用print()函数打印数组的shape得到的也确实是tuple，不过list貌似也行
#[]或者()代表不是数组，而是普通变量，例如int或str
data = tf.get_variable('data', shape=[])
data2 = tf.get_variable('data2', shape=())
#tf.get_variable()函数无法使用shape=[None]来初始化变量，因为这个函数无法通过传入一个常量告诉函数这个变量的shape是什么
#dataarr0 = tf.get_variable('dataarr0', shape=[None], initializer=[1, 2])
#placeholder变量可以使用shape=[None]，然后等到sess.run()的时候通过feed_dict设置值，设置的数组是几维，placeholder对象就会被初始化为几维
#[None]代表一维不定长数组
ph0 = tf.placeholder(tf.int32, shape=[None])
#[None, None]代表二维不定长数组
ph1 = tf.placeholder(tf.int32, shape=[None, None])
#[1]或者(1)代表这是长度为1的一维数组
dataarr1 = tf.get_variable('dataarr1', shape=[1])
dataarr1_1 = tf.get_variable('dataarr1_1', shape=(1))
#二维数组，第一维有2个成员，第二维是一个长度为3的一维数组
dataarr2 = tf.get_variable('dataarr2', shape=[2, 3])
dataarr2_1 = tf.get_variable('dataarr2_1', shape=(2, 3))
#对数据进行reshape操作，初始化data3为一维数组，共12个成员
data3 = tf.Variable([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], tf.int32)
#reshape成三维数组，按照第一维维度为2，第三维维度为3，中间第二维维度通过x的长度和第一维、第三维维度相除计算出来
data3_reshape = tf.reshape(data3, [2, -1, 3])
#由于data3_reshape是三维数组，因此0, 2, 1代表对第二维和第三维数据进行转置
data3_transpose1 = tf.transpose(data3_reshape, [0, 2, 1])
data3_transpose2 = tf.transpose(data3_reshape, [2, 1, 0])
#x.set_shape([2, 3, 2])
with tf.Session() as sess:
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    print('data value: ', sess.run(data))
    print('data shape: ', data.shape)
    print('data2 value: ', sess.run(data2))
    print('data2 shape: ', data2.shape)
    #print('dataarr0 value: ', sess.run(dataarr0))
    #print('dataarr0 shape: ', dataarr0.shape)
    print('ph0 value: ', sess.run(ph0, feed_dict={ph0: [2, 4, 6]}))
    #初始化的值不能是[[1, 3], [2, 4, 6]]，也就是说必须输入一个矩阵
    print('ph1 value: ', sess.run(ph1, feed_dict={ph1: [[1, 3, 5], [2, 4, 6]]}))
    print('dataarr1 value: ', sess.run(dataarr1))
    print('dataarr1 shape: ', dataarr1.shape)
    print('dataarr1_1 value: ', sess.run(dataarr1_1))
    print('dataarr1_1 shape: ', dataarr1_1.shape)
    print('dataarr2 value: ', sess.run(dataarr2))
    print('dataarr2 shape: ', dataarr2.shape)
    print('dataarr2_1 value: ', sess.run(dataarr2_1))
    print('dataarr2_1 shape: ', dataarr2_1.shape)
    print('data3 shape: ', data3.shape)
    print('data3_reshape shape: ', data3_reshape.shape)
    print('data3_reshape: ', sess.run(data3_reshape))
    print('data3_transpose1: ', sess.run(data3_transpose1))
    print('data3_transpose2: ', sess.run(data3_transpose2))