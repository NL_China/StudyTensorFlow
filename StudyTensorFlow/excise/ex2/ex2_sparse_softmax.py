#练习2是逻辑回归，采用sparse_softmax
import tensorflow as tf
import csv
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

import utils.data as data

def readData():
    all_data = []
    filepath = os.path.join(sys.path[0], "ex2data1.txt")
    #通过python的csv模块读取csv文件，而不是通过tensorflow读取csv文件
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            newrow = [float(row[0]), float(row[1]), float(row[2]), 1 - float(row[2])]
            print(newrow)
            all_data.append(newrow)

    all_array = np.array(all_data)
    np.random.shuffle(all_array)
    return all_array

def drawfigure(all_array, w_data, b_data):
    f1 = plt.figure(1)
    plt.ion()
    #通过boolean索引来从全部数据中筛选出正值和负值数据集
    #all_array[:,2] == '1'意思是这个二维数组的第二维的第三个成员为字符'1'的那些行索引，所以得到的这个行索引要放在切片的第一个参数，这个索引其实是第一维数据的索引
    plt.scatter(all_array[all_array[:,2] == 1.0, 0], all_array[all_array[:,2] == 1.0, 1], s=10, c='r', marker='o')
    plt.scatter(all_array[all_array[:,2] == 0.0, 0], all_array[all_array[:,2] == 0.0, 1], s=10, c='b', marker='x')
    #画决策边界
    x_min = np.min(all_array[:,0])
    y_min = -(w_data[0, 0] * x_min + b_data[0, 0])/w_data[0, 1]
    x_max = np.max(all_array[:,0])
    y_max = -(w_data[0, 0] * x_max + b_data[0, 0])/w_data[0, 1]
    x_line = [x_min, x_max]
    y_line = [y_min, y_max]
    plt.plot(x_line, y_line)
    plt.show()

def LogisticRegression(all_array, sess):
    rows = len(all_array)
    #使用tensorflow的l2_normalize进行归一化，但是效果不太好，训练了很久，代价函数收敛得非常慢，同样是learning rate: 0.5，训练了20000次代价函数值才到0.204，而z-score归一化则训练了500次就到了0.203（matlab版本代价函数训练完后收敛到的值），我觉得是因为这种归一化方式导致数据都变得很小
    #后改为learning rate: 5，训练2000次代价函数的值达到了0.204，也有可能和sparse_softmax_cross_entropy_with_logits函数自身的特点有关？
    data_norm = tf.nn.l2_normalize(all_array[:, 0:2], dim=0)
    data_norm_result = sess.run(data_norm)
    print(data_norm_result)
    label_data = all_array[:, 2:3].reshape(rows)
    #使用placeholder定义输入数据
    data = tf.placeholder(tf.float32, shape=(rows, 2), name='data')
    label = tf.placeholder(tf.int32, shape=(rows), name='label')
    #使用Variable定义weights和bias
    w_init = tf.constant(0.0, shape=[2, 2], dtype=tf.float32)
    w = tf.Variable(initial_value=w_init, name='weight')
    b_init = tf.constant(0.0, shape=[1, 2], dtype=tf.float32)
    b = tf.Variable(initial_value=b_init, name='bias')
    #逻辑回归代价函数
    #注意：网上找来的代码
    #但是如果要用下面的softmax那么就得把输出的分类变成2分类，现在输出只有一个值，1代表正值，0代表负值，这样是不能用softmax的
    #所以前面在处理输入数据的时候加上了第四列
    y = tf.add(tf.matmul(data, tf.transpose(w)), b)
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels = label, logits = y)
    loss = tf.reduce_mean(cross_entropy)
    
    #训练
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    #初始化状态下的代价值
    print('w shape: ', w.shape)
    print('b shape: ', b.shape)
    print('init: ', sess.run(w), sess.run(b))
    #data_value = all_array[:,0:2]
    print('loss: ', sess.run(loss, feed_dict={data: data_norm_result, label: label_data}))
    print('cross_entropy: ', sess.run(cross_entropy, feed_dict={data: data_norm_result, label: label_data}))
    
    gd = tf.train.GradientDescentOptimizer(learning_rate=5).minimize(loss)
    for step in range(2000):
        ret = sess.run(gd, feed_dict={data: data_norm_result, label: label_data})
        if step % 10 == 0:
            print(step, sess.run(w), sess.run(b))
            print(step, 'loss: ', sess.run(loss, feed_dict={data: data_norm_result, label: label_data}))
    #将归一化后的值写回原数组
    all_array[:, 0:2] = data_norm_result
    return all_array, sess.run(w), sess.run(b)


with tf.Session() as sess:
    all_array = readData()
    #在LogisticRegression内部对all_array调用了tf.nn.l2_normalize进行归一化操作
    data_all, w_data, b_data = LogisticRegression(all_array, sess)
    drawfigure(data_all, w_data, b_data)
    os.system("pause")
