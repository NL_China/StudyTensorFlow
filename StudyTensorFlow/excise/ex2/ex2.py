#练习2是逻辑回归
import tensorflow as tf
import csv
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import utils.data as data

def readData():
    all_data = []
    filepath = os.path.join(sys.path[0], "ex2data1.txt")
    #通过python的csv模块读取csv文件，而不是通过tensorflow读取csv文件
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            print(row)
            all_data.append([float(row[0]), float(row[1]), float(row[2])])

    all_array = np.array(all_data)
    return all_array

def drawfigure(all_array, w_data, b_data):
    f1 = plt.figure(1)
    plt.ion()
    #通过boolean索引来从全部数据中筛选出正值和负值数据集
    #all_array[:,2] == '1'意思是这个二维数组的第二维的第三个成员为字符'1'的那些行索引，所以得到的这个行索引要放在切片的第一个参数，这个索引其实是第一维数据的索引
    plt.scatter(all_array[all_array[:,2] == 1.0, 0], all_array[all_array[:,2] == 1.0, 1], s=10, c='r', marker='o')
    plt.scatter(all_array[all_array[:,2] == 0.0, 0], all_array[all_array[:,2] == 0.0, 1], s=10, c='b', marker='x')
    #画决策边界
    x_min = np.min(all_array[:,0])
    y_min = -(w_data[0, 0] * x_min + b_data[0, 0])/w_data[0, 1]
    x_max = np.max(all_array[:,0])
    y_max = -(w_data[0, 0] * x_max + b_data[0, 0])/w_data[0, 1]
    x_line = [x_min, x_max]
    y_line = [y_min, y_max]
    plt.plot(x_line, y_line)
    plt.show()

def LogisticRegression(all_array, sess):
    rows = len(all_array)
    #使用placeholder定义输入数据
    data = tf.placeholder(tf.float32, shape=(rows, 2), name='data')
    label = tf.placeholder(tf.float32, shape=(rows, 1), name='label')
    #使用Variable定义weights和bias
    w_init = tf.constant(0.0, shape=[1, 2], dtype=tf.float32)
    w = tf.Variable(initial_value=w_init, name='weight')
    b_init = tf.constant(0.0, shape=[1, 1], dtype=tf.float32)
    b = tf.Variable(initial_value=b_init, name='bias')
    #逻辑回归代价函数
    #tf.matmul()是矩阵相乘，注意：必须都是矩阵才行，不能矩阵和向量相乘，例如w是向量[2]，data是矩阵[100, 2]，把data
    #转置后变为[2, 100]，但是向量[2]，需要变成[1, 2]才可以，就是说都是二位数组才行，两个矩阵的维度需要相同
    y = 1.0 / (1.0 + tf.exp(-(tf.add(tf.matmul(data, tf.transpose(w)), b))))
    #y变成了[1, 100]数组，而label实际上是[100, 1]数组
    #想起来，吴恩达的课程中提到过，下面这个代价函数有多个局部最优解，因此不能用传统的梯度下降算法，需要用拟牛顿法等方法来寻找全局最优解
    #但是tensorflow中没有提供像matlab那样的fminunc函数来求解这个代价函数，所以还是使用了tensorflow最基本的梯度下降函数GradientDescentOptimizer
    loss = -tf.reduce_mean(tf.multiply(label, tf.log(y)) + tf.multiply(1-label, tf.log(1-y)))
    loss2 = -(tf.multiply(label, tf.log(y)) + tf.multiply(1-label, tf.log(1-y)))
    #训练
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    print('all_array: ', all_array)
    #初始化状态下的代价值
    print('w shape: ', w.shape)
    print('b shape: ', b.shape)
    print('init: ', sess.run(w), sess.run(b))
    print('loss: ', sess.run(loss, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:3]}))
    #loss2_value = sess.run(loss2, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:3]})
    #print('loss2: ', loss2_value)
    #print('loss2 mean: ', sess.run(tf.reduce_mean(loss2_value)))
    #
    gd = tf.train.GradientDescentOptimizer(learning_rate=0.5).minimize(loss)
    for step in range(500):
        ret = sess.run(gd, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:3]})
        if step % 10 == 0:
            print(step, sess.run(w), sess.run(b))
            print(step, 'loss: ', sess.run(loss, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:3]}))
    #画线

    return sess.run(w), sess.run(b)


with tf.Session() as sess:
    all_array = readData()
    all_array_new = data.normalize(all_array, 2)
    w_data, b_data = LogisticRegression(all_array_new, sess)
    drawfigure(all_array_new, w_data, b_data)
    os.system("pause")
