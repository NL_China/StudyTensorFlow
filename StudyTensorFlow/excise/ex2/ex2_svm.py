import tensorflow as tf
import sys
sys.path.append('D:/projects/StudyTensorFlow/StudyTensorFlow/')
import utils.data as datautil
import numpy as np
import matplotlib.pyplot as plt

def drawfigure(all_array, w_data, b_data):
    f1 = plt.figure(1)
    plt.ion()
    #通过boolean索引来从全部数据中筛选出正值和负值数据集
    #all_array[:,2] == '1'意思是这个二维数组的第二维的第三个成员为字符'1'的那些行索引，所以得到的这个行索引要放在切片的第一个参数，这个索引其实是第一维数据的索引
    plt.scatter(all_array[all_array[:,2] == 1.0, 0], all_array[all_array[:,2] == 1.0, 1], s=10, c='r', marker='o')
    plt.scatter(all_array[all_array[:,2] == -1.0, 0], all_array[all_array[:,2] == -1.0, 1], s=10, c='b', marker='x')
    #画决策边界
    x_min = np.min(all_array[:,0])
    y_min = -(w_data[0, 0] * x_min + b_data[0])/w_data[1, 0]
    x_max = np.max(all_array[:,0])
    y_max = -(w_data[0, 0] * x_max + b_data[0])/w_data[1, 0]
    x_line = [x_min, x_max]
    y_line = [y_min, y_max]
    plt.plot(x_line, y_line)
    plt.show()

def svm(all_array, sess):
     #使用placeholder定义输入数据
    data = tf.placeholder(tf.float32, shape=(None, 2), name='data')
    label = tf.placeholder(tf.float32, shape=(None, 1), name='label')
    #使用Variable定义weights和bias
    w_init = tf.constant(0.0, shape=[2, 1], dtype=tf.float32)
    w = tf.Variable(initial_value=w_init, dtype=tf.float32, name='weight')
    b_init = tf.constant(0.0, shape=[1], dtype=tf.float32)
    b = tf.Variable(initial_value=b_init, dtype=tf.float32, name='bias')
    y = tf.add(tf.matmul(data, w), b)
    c = 0.2
    #svm的代价函数
    loss = tf.reduce_sum(tf.nn.relu(1 - label * y)) + c * tf.nn.l2_loss(w)
    gd = tf.train.AdamOptimizer(learning_rate=0.5).minimize(loss)
    #使用GradientDescentOptmizer的版本，必须除以训练集的数量，上面的AdamOptimizer不需要除以训练集的数量
    #loss = (tf.reduce_sum(tf.nn.relu(1 - label * y)) + c * tf.nn.l2_loss(w)) / 100
    #gd = tf.train.GradientDescentOptimizer(learning_rate=0.5).minimize(loss)

    data_data = all_array[:, 0:2]
    label_data = all_array[:, 2:3]

    #训练
    initializer = tf.global_variables_initializer()
    sess.run(initializer)

    #初始化状态下的代价值
    print('w shape: ', w.shape)
    print('b shape: ', b.shape)
    print('init: ', sess.run(w), sess.run(b))
    print('loss: ', sess.run(loss, feed_dict={data: data_data, label: label_data}))

    for step in range(500):
        ret = sess.run(gd, feed_dict={data: data_data, label: label_data})
        if step % 10 == 0:
            print(step, sess.run(w), sess.run(b))
            print(step, 'loss: ', sess.run(loss, feed_dict={data: data_data, label: label_data}))
    return sess.run(w), sess.run(b)

with tf.Session() as sess:
    all_array = datautil.readCsvData(sys.path[0], 'ex2data1.txt', True, True)
    #将标签数据处理成1和-1,也就是把0改为-1
    all_array[all_array[:, 2] == 0, 2] = -1
    all_array_new = datautil.normalize(all_array, 2)
    w_data, b_data = svm(all_array_new, sess)
    drawfigure(all_array_new, w_data, b_data)