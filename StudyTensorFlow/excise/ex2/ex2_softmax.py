#练习2是逻辑回归，采用softmax
from __future__ import division
import tensorflow as tf
import csv
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import utils.data as data

#花费了很长时间才明白这个项目之所以梯度下降到后来代价函数的值总是在几个值之间跳跃，而且这些值甚至比初始化时的代价函数值还高是因为没有对训练集进行归一化
#在main函数中调用了z-score归一化data.normalize()

def readData():
    all_data = []
    filepath = os.path.join(sys.path[0], "ex2data1.txt")
    #通过python的csv模块读取csv文件，而不是通过tensorflow读取csv文件
    with open(filepath, 'r') as f:
        reader = csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONE)
        for row in reader:
            #新增一列，因为softmax要求标签数据的格式应该是one-hot number，如果是3分类问题，那么3个类别分别表示为[1, 0, 0], [0, 1, 0], [0, 0, 1]
            newrow = [float(row[0]), float(row[1]), float(row[2]), 1 - float(row[2])]
            print(newrow)
            all_data.append(newrow)

    all_array = np.array(all_data)
    #np.random.shuffle(all_array)
    return all_array

def drawfigure(all_array, w_data, b_data):
    f1 = plt.figure(1)
    plt.ion()
    #通过boolean索引来从全部数据中筛选出正值和负值数据集
    #all_array[:,2] == '1'意思是这个二维数组的第二维的第三个成员为字符'1'的那些行索引，所以得到的这个行索引要放在切片的第一个参数，这个索引其实是第一维数据的索引
    plt.scatter(all_array[all_array[:,2] == 1.0, 0], all_array[all_array[:,2] == 1.0, 1], s=10, c='r', marker='o')
    plt.scatter(all_array[all_array[:,2] == 0.0, 0], all_array[all_array[:,2] == 0.0, 1], s=10, c='b', marker='x')
    #画决策边界
    x_min = np.min(all_array[:,0])
    y_min = -(w_data[0, 0] * x_min + b_data[0])/w_data[1, 0]
    x_max = np.max(all_array[:,0])
    y_max = -(w_data[0, 0] * x_max + b_data[0])/w_data[1, 0]
    x_line = [x_min, x_max]
    y_line = [y_min, y_max]
    plt.plot(x_line, y_line)
    plt.show()

def LogisticRegression(all_array, sess):
    rows = len(all_array)
    #使用placeholder定义输入数据
    data = tf.placeholder(tf.float32, shape=(None, 2), name='data')
    label = tf.placeholder(tf.float32, shape=(None, 2), name='label')
    #使用Variable定义weights和bias
    w_init = tf.constant(0.0, shape=[2, 2], dtype=tf.float32)
    w = tf.Variable(initial_value=w_init, dtype=tf.float32, name='weight')
    b_init = tf.constant(0.0, shape=[2], dtype=tf.float32)
    b = tf.Variable(initial_value=b_init, dtype=tf.float32, name='bias')
    #逻辑回归代价函数
    #注意：网上找来的代码
    #但是如果要用下面的softmax那么就得把输出的分类变成2分类，现在输出只有一个值，1代表正值，0代表负值，这样是不能用softmax的
    #所以前面在处理输入数据的时候加上了第四列
    y = tf.add(tf.matmul(data, w), b)
    #试验了很久才搞明白为什么我一开始用什么样的初始化值得到的loss都是0.693174，原来不能用同一个值初始化w，还有b，例如w为[[0.2, 0.2],[0.2, 0.2]]，b为[[-25., 25.]]
    #这样初始化是不行的，这样和w: [[0, 0], [0, 0]]，b: [[0, 0]]是同样的初始化方式，可能是因为所有的值都相同，softmax_cross_entropy_with_logits计算时给转成0了
    #原因是softmax回归对于参数减去同一个值(如果权重是矩阵的话，例如2x2矩阵，那么减掉的是一个向量[2])得到的结果是相同的，所以用0.2初始化的结果和用0初始化的结果相同
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels = label, logits = y)
    lamda = 3.0
    loss = tf.reduce_mean(cross_entropy)
    #loss = -tf.reduce_mean(label * tf.log(tf.nn.softmax(y))) + lamda / 2 * tf.reduce_sum(w * w)
    
    #训练
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    print('all_array: ', all_array)
    #初始化状态下的代价值
    print('w shape: ', w.shape)
    print('b shape: ', b.shape)
    print('init: ', sess.run(w), sess.run(b))
    print('loss: ', sess.run(loss, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:4]}))
    
    gd = tf.train.GradientDescentOptimizer(learning_rate=0.5).minimize(loss)
    for step in range(500):
        ret = sess.run(gd, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:4]})
        if step % 10 == 0:
            print(step, sess.run(w), sess.run(b))
            print(step, 'loss: ', sess.run(loss, feed_dict={data: all_array[:, 0:2], label: all_array[:, 2:4]}))
    return sess.run(w), sess.run(b)


with tf.Session() as sess:
    all_array = readData()
    all_array_new = data.normalize(all_array, 2)

    w_data, b_data = LogisticRegression(all_array_new, sess)
    #画散点图和决策边界
    drawfigure(all_array_new, w_data, b_data)

    os.system("pause")
