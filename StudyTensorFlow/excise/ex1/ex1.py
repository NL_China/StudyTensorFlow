import tensorflow as tf
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

#一开始参数给的是/data/ex1data1.txt，报错：The system cannot find the path specified. 改成绝对路径就好了，不过还是要研究一下相对路径的给法
filename_queue = tf.train.string_input_producer([os.path.join(sys.path[0], "ex1data1.txt")])
reader = tf.TextLineReader()
key, value = reader.read(filename_queue)
#如果csv文件是M行N列，那么record_defaults就应该是一个N*1的数组，1.0代表数据是float，1代表是int，string不清楚
record_defaults = [[1.0], [1.0]]
#csv文件中有几列，这里就要写几个参数
col1, col2 = tf.decode_csv(
    value, record_defaults=record_defaults)

#线性回归
def LinearRegression(x_data, y_data, sess):
    rows = len(x_data)
    #用placeholder定义输入数据
    #所有的数据都最好使用float32，float16得到的结果和Octave计算出来的结果有偏差，Octave应该用的也是float32
    data = tf.placeholder(tf.float32, shape=[rows], name='data')
    label = tf.placeholder(tf.float32, shape=[rows], name='label')
    #用Variable定义变量
    w_init = tf.constant(0.0, shape=[1], dtype=tf.float32)
    w = tf.Variable(initial_value=w_init, name='weights')
    b_init = tf.constant(0.0, shape=[1], dtype=tf.float32)
    b = tf.Variable(initial_value=b_init, name='bias')
    #计算代价函数
    y_cal = tf.add(tf.multiply(w, data), b)
    #代价函数需要除以2再除以训练集数量
    loss = tf.reduce_sum(tf.square(y_cal - y_data)) / 2.0 / rows
    #用梯度下降算法训练
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    #初始化状态下的代价值
    print('loss: ', sess.run(loss, feed_dict={data: x_data, label: y_data}))
    print('w shape: ', w.shape)
    print('b shape: ', b.shape)

    print('init: ', sess.run(w), sess.run(b))
    #learning rate是0.01，梯度下降迭代次数是1500，都是来自matlab版的作业
    gd = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(loss)
    for step in range(1501):
        ret = sess.run(gd, feed_dict={data: x_data, label: y_data})
        if step % 100 == 0:
            print(step, sess.run(w), sess.run(b))
    w_data = sess.run(w)
    b_data = sess.run(b)
    plt.ion()
    drawfigure(x_data, y_data, w_data, b_data)
    #画等高线
    w_arr = np.linspace(-1, 4, 100)
    b_arr = np.linspace(-10, 10, 100)
    loss_arr = np.zeros(shape=(100, 100))
    w_index = 0
    b_index = 0
    for b_member in b_arr:
        w_index = 0
        for w_member in w_arr:
            loss_arr[w_index, b_index] = computeCost(w_member, b_member, x_data, y_data)
            w_index += 1
        b_index += 1
    f2 = plt.figure(2)
    #contour(X, Y, Z, N)
    #X和Y都是一维数组，Z是一个二维数组，Z的列数和X的维度相同，因此Z的列index就是X的index，Z的行数和Y的维度相同，Z的行index就是Y的index
    #第四个参数可以传入一个数组，代表想在哪些高度画出线来
    plt.contour(b_arr, w_arr, loss_arr, [1, 3, 10, 30, 100, 300], colors = 'black', linewidth = 0.5)
    plt.ioff()
    plt.show()

#画出数据的散点图和拟合后的直线
def drawfigure(x_data, y_data, w_data, b_data):
    f1 = plt.figure(1)
    #打开交互模式，否则plt.show()显示图像后会阻塞进程，直到图像关闭后才开始执行后面的训练
    #plt.ion()
    #plt.subplot(211)  
    plt.scatter(x_data, y_data)
    #画直线
    x_min = np.min(x_data)
    y_min = x_min * w_data + b_data
    x_max = np.max(x_data)
    y_max = w_data * x_max + b_data
    x_line = [x_min, x_max]
    y_line = [y_min, y_max]
    plt.plot(x_line, y_line)
    #plt.ioff()
    plt.show()

def computeCost(w, b, data, label):
    rows = len(data)
    y = w * data + b
    return np.sum(np.square(y - label)) / 2.0 / rows

with tf.Session() as sess:
    print(sys.path[0])
    # Start populating the filename queue.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    x = []
    y = []
    for i in range(97):
        # Retrieve a single instance:
        ret = sess.run([col1, col2])
        x.append(ret[0])
        y.append(ret[1])
    coord.request_stop()
    coord.join(threads)
    #tensorflow里面的数据集用的都是numpy的数组
    x_data = np.array(x)
    y_data = np.array(y)

    LinearRegression(x_data, y_data, sess)

