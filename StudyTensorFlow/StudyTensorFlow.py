import tensorflow as tf

#这个函数是从mnist_deep.py中拷贝出来的，试验了一下，没有变量共享，因为是Variable函数生成的
def weight_variable(shape):
    """weight_variable generates a weight variable of a given shape."""
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

W_conv1 = weight_variable([5, 5, 1])
W_conv2 = weight_variable([5, 5, 2])
global_init = tf.global_variables_initializer()
with tf.Session() as sess:    
    sess.run(global_init)
    vs = tf.trainable_variables()
    print('There are %d train_able_variables in the Graph: ' % len(vs))
    for v in vs:
        print(v)

