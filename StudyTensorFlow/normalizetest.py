import tensorflow as tf
import numpy as np

with tf.Session() as sess:
    data1 = np.arange(4, dtype=np.float16)
    data1_norm = tf.nn.l2_normalize(data1, dim=[0])
    #data1_norm2 = tf.nn.l2_normalize(data1, dim=[1])
    print(sess.run(data1_norm))
    #print(sess.run(data1_norm2))
    data2 = np.arange(4, dtype=np.float16).reshape([2, 2])
    #data2 = [[0 1] [2 3]]
    #如果数组是2维数组，那么dim=1代表在最后一级上进行归一化，所以是0和1归一化，2和3归一化，因为最后一级数据分别是[0 1]和[2 3]
    #dim=0代表的是最开始一维的维度的数据，在这一维度上的归一化，所以是0和2归一化，1和3归一化
    #对于矩阵来说，dim=0代表的是按列进行归一化，dim=1代表的是按行进行归一化
    data2_norm = tf.nn.l2_normalize(data2, dim=[0])
    data2_norm1 = tf.nn.l2_normalize(data2, dim=[1])
    print(data2)
    print(sess.run(data2_norm))
    print(sess.run(data2_norm1))
    data3 = np.array([0, 1, 2, 3, 0, 1, 2, 3], dtype=np.float16).reshape([2, 2, 2])
    #data3 = [[[0 1] [2 3]] [[0 1] [2 3]]]，8个元素的坐标分别为(0, 0, 0), (0, 0, 1), (0, 1, 0), (0, 1, 1), (1, 0, 0), (1, 0, 1), (1, 1, 0), (1, 1, 1)
    #dim代表的是除了dim的值之外的维度都相同的数据进行归一化
    #dim=2代表在最后一个维度上的归一化，也就是其他两个维度都相同，最后一个维度不同的数据进行归一化，所以是(0, 0, 0), (0, 0, 1)归一化，(0, 1, 0), (0, 1, 1)归一化，(1, 0, 0), (1, 0, 1)归一化，(1, 1, 0), (1, 1, 1)归一化
    #dim=1代表在中间一个维度上的归一化，也就是第1和第3个维度相同，中间一个维度不同的数据进行归一化，所以是(0, 0, 0), (0, 1, 0)归一化，(0, 0, 1), (0, 1, 1)归一化，(1, 0, 0), (1, 1, 0)归一化，(1, 0, 1), (1, 1, 1)归一化
    #dim=0代表在第一个维度上的归一化，也就是第2和第3个维度相同，第一个维度不同的数据进行归一化，所以是(0, 0, 0), (1, 0, 0)归一化，(0, 0, 1), (1, 0, 1)归一化，(0, 1, 0), (1, 1, 0)归一化，(0, 1, 1), (1, 1, 1)归一化
    data3_norm = tf.nn.l2_normalize(data3, dim=[0])
    data3_norm1 = tf.nn.l2_normalize(data3, dim=[1])
    data3_norm2 = tf.nn.l2_normalize(data3 , dim=[2])
    print(data3)
    print(sess.run(data3_norm))
    print(sess.run(data3_norm1))
    print(sess.run(data3_norm2))
'''
感觉l2_normalize这种方式在数据量比较大的时候会把数据缩得很小，例如数组有10个成员1到10，那么使用了ls_normalize后10就变成了0.5，而不是我想象的1。至于原来的成员1，则会变得非常小。
'''

