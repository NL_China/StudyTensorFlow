# StudyTensorFlow

---

学习TensorFlow的一些基本的API，同时将https://www.coursera.org/ &nbsp;上吴恩达的斯坦福大学《机器学习》课程中的作业使用Python3+TensorFlow做一遍。



## 目录结构

根目录下的*.py文件为TensorFlow API的一些练习和实验；

excise文件夹下为《机器学习》课程作业的TensorFlow版本，目前只完成了前两个作业，其中ex2中：ex2.py是与《机器学习》课程作业2中的程序结构相同，采用了传统的逻辑回归算法；ex2\_softmax.py采用了softmax的版本；ex2\_svm.py是采用支持向量机算法的版本；



## 环境

TensorFlow 1.x

Python 3

Numpy

Matplotlib